# Iron Heart

## Introduction

Welcome to the **Iron Heart** project! Inspired by the iconic Arc Reactor from Iron Man, this project aims to create a wearable heart rate monitor. The device uses a Raspberry Pi Pico W to measure heart rate and visualize it using NeoPixel LEDs. It also hosts a captive portal that displays real-time heart rate data on a web page, accessible from any mobile device connected to its Wi-Fi network.

![Iron Heart in Action](images/IMG_4736.jpeg)

## Project Structure

The project is organized into the following folders:

- **SW (Software)**
  - Contains all the program files needed to run the heart rate monitor and web server on the Raspberry Pi Pico W.
- **HW (Hardware)**
  - Contains all the hardware design files.
  - **PCB**
    - Contains the PCB design files for the heart rate monitor.
  - **3D_models**
    - Contains 3D design files for the enclosure and other mechanical components.

All folders contains documentation for their respected parts.

## Showcase

### The Wearable Device

![Iron Heart Device](images/IMG_4734.jpeg)

### Full Device Setup

![Iron Heart Full Setup](images/IMG_4735.jpeg)

### Ready to Wear

![Iron Heart Ready to Wear](images/IMG_4733.jpeg)

## Usage

1. **Set up Arduino IDE for Raspberry Pi Pico W:**

   - Follow the instructions from the [official guide](https://www.raspberrypi.org/documentation/microcontrollers/micropython.html) to set up the Arduino IDE for the Raspberry Pi Pico W.

2. **Install Required Libraries:**

   - Install the following libraries via the Arduino Library Manager:
     - Adafruit NeoPixel
     - WiFi
     - WebServer
     - DNSServer

3. **Flash the Code:**

   - Connect your Raspberry Pi Pico W to your computer.
   - Open the provided `src.ino` file in the Arduino IDE (located in the `SW` folder).
   - Select the correct board and port from the `Tools` menu.
   - Click on the `Upload` button to flash the code to the Pico W.

4. **Power the Pico W:**

   - After flashing the code, power the Raspberry Pi Pico W using a USB cable or an external power source.

5. **Connect to Wi-Fi:**

   - On your mobile device or computer, connect to the Wi-Fi network named `IRON_HEART` with the password `1234567899`.

6. **Automatic Redirection:**
   - Once connected, the device should automatically redirect to the heart rate monitor web page. If not, open a web browser and navigate to `http://192.168.4.1`.
