# 3D Documentation

## 3D Assembly Requirements

Required software to open the original documentation:
 - Autodesk Inventor Professional 2023
 - PrusaSlicer 2.7.0

Required machines:
 - 3D Printer
 - 2D Laser cut machine - support for cutting plexiglass

Required materials:
 - PET-G filament - 200g
 - Plexiglass - 110x110 mm, diffuse 
 - Textile belt - 3 m
 - Screws M2x6mm - 14 Pcs
 - Screws 3x10mm - 8 Pcs

## 3D Printing

The folder 'Print profiles' contains all .gcode files with set profile for Prusa MK3S+ printer and PET-G filament.

You can use the models stored in the folder 'Export files' to print on another printer. The recommended print settings for models are as follows:

 - IronHeart_battery_holder
   - Layer height - 0.2 mm
   - Fill density - 20 %
   - Fill pattern - Cubic
   - Supports - YES
   - Support style - Organic
   - Speed - 100 %

![Battery holder](images/battery_holder.PNG)

- IronHeart_belt_holder - 2x
   - Layer height - 0.2 mm
   - Fill density - 20 %
   - Fill pattern - Cubic
   - Supports - NO
   - Speed - 100 %

![Belt holder](images/belt_holder.PNG)

- IronHeart_Case
   - Layer height - 0.2 mm
   - Fill density - 30 %
   - Fill pattern - Cubic
   - Supports - YES
   - Support style - Organic
   - Speed - 90 %

![Main case](images/holder.PNG)

- IronHeart_Pattern
   - Layer height - 0.2 mm
   - Fill density - 20 %
   - Fill pattern - Cubic
   - Supports - NO
   - Speed - 100 %

![Pattern](images/pattern.PNG)

## Assembly Procedure

1. **Pattern print**
    - The diffuse Plexiglas is already inserted into the 3D model during printing. It is therefore necessary to add a pause in a certain layer when creating the print program. When the pause is taken, the Plexiglas is inserted into the model and the whole model is allowed to finish.

2. **Textile strips**
    - Take the two textile strips and fold them over each other and join around the edges. It is necessary to create a space inside that will lead the wires.
    - Make two of these strips and attach the buckle to their ends.
    - Make two holes in one strip about 5 cm and 20 cm from the beginning. This is where the probes will lead out.
    - Make a hole in the second strip for the probe at a distance of 5 cm from the beginning (for probe 3) and a hole at the end for the power cables.

3. **Preparing the wiring**
    - Insert the necessary probes and power supply wires into the prepared holes.

4. **Final assembly**
    - Use M2x6 screws to attach the Pattern 3D part to the PCB.
    - Connect the probes to the PCB together with the power supply and insert it into the case 3D part.
    - Finally, place the fabric belts in the prepared holes and attach it to the case using the belt holder 3D part and 3x10 screws.
