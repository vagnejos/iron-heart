# Schematic

During the design of the schematic, individual components for the project were selected. For the LEDs, **WS2812-V4** was chosen. These LEDs are powered by 5V and can be connected in series thanks to a daisy-chain bus. The downside of these LEDs is the need for a parallel 100 nF capacitor for each LED. The newer version, WS2812-V5, includes an internal capacitor; however, this version was not available during the project implementation.

The control unit will be a **Raspberry Pi Pico** development board with an **RP2040** processor. The project will be powered by **4x AAA batteries**, providing a total voltage of 6V in series. To minimize the total number of components, the battery power will be connected to the power pin of the WS2812, which allows for a maximum voltage of 7V. The batteries are connected to the Raspberry Pi via the **VUSB pin**, which has a built-in 3.3V regulator for powering the Raspberry Pi, eliminating the need for an external regulator.

The WS2812 LEDs are connected to **pins 2, 3, and 4**. The heart rate sensor is powered by 3.3V and connected to the Raspberry Pi via **pins 26 and 27**.

Final schematic can be found in the file Schematic.pdf.

![LED Layout](Schematic.png)

# PCB

The placement of individual components is dependent on the 3D model, which precisely specifies the position of each LED, the Raspberry Pi, and the heart rate sensor. The data traces have a width of **0.254 mm**, and the power traces for the WS2812 LEDs have a width of **0.5 mm**. This width is sufficient for the components, as the maximum current draw for the LEDs is 36 mA. The maximum number of LEDs connected on one trace is 20, corresponding to a current of 720 mA. The PCB implements a **poured copper method**, specifically a poured ground plane, to improve the EMC properties of the PCB. The arrangement of the LEDs can be seen in the image below.

![LED Layout](PCB_top.png)
