#include <Adafruit_NeoPixel.h>
#include <WiFi.h>
#include <DNSServer.h>
#include <WebServer.h>
#include "hardware/uart.h"
#include "pico/stdlib.h"

// Define constants for LED strip
#define DIN1 2
#define COUNT1 15
Adafruit_NeoPixel line1 = Adafruit_NeoPixel(COUNT1, DIN1, NEO_GRB + NEO_KHZ800);

#define DIN2 3
#define COUNT2 21
Adafruit_NeoPixel line2 = Adafruit_NeoPixel(COUNT2, DIN2, NEO_GRB + NEO_KHZ800);

#define DIN3 4
#define COUNT3 3
Adafruit_NeoPixel line3 = Adafruit_NeoPixel(COUNT3, DIN3, NEO_GRB + NEO_KHZ800);

// Define constants for filtering
#define LP_ALPHA 0.1
#define HP_ALPHA 0.9

#define EKG_PIN 28

// Define WiFi credentials
const char* ssid = "IRON_HEART";
const char* password = "1234567899";

WebServer server(80);
DNSServer dnsServer;

// Define variables for filtering
float lowPassValue = 0;
float highPassValue = 0;
float prevValue = 0;

volatile float heartRate = 0.0;
float prevHeartRate = 0.0;

// Peak detection parameters
#define WINDOW_SIZE 3
float values[WINDOW_SIZE];
int valueIndex = 0;

#define THRESHOLD 0.5     // Adjust threshold as needed
#define MIN_INTERVAL 500  // Minimum time between peaks (in milliseconds)

unsigned long lastPeakTime = 0;

long brightness = 0;
long previousBrightness = 0;

float filteredValue;
int rawValue;

// Function prototypes
float lowPassFilter(float input);
float highPassFilter(float input);
bool detectPeak(float current);
void controlLEDs(long brightness, float heartRate);
void handleRoot();
void handleNotFound();
void serveHtml();
void redirectToCaptivePortal();

void setup() {
  Serial.begin(115200);
  delay(10);
  pinMode(EKG_PIN, INPUT);
  Serial.println("Core 0 setup finished");

  // Initialize LED strips
  line1.begin();
  line1.fill(line1.Color(0, 0, 0), 0, COUNT1);
  line1.show();
  Serial.println("Core 0 setup finished");

  line2.begin();
  line2.fill(line2.Color(0, 0, 0), 0, COUNT2);
  line2.show();
  Serial.println("Core 0 setup finished");

  line3.begin();
  line3.fill(line3.Color(0, 0, 0), 0, COUNT3);
  line3.show();
  Serial.println("Core 0 setup finished");

  Serial.println("Core 0 setup finished");
  delay(5000);
}

void setup1() {
  delay(5000);
  // Start WiFi in AP mode
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  dnsServer.start(53, "*", IP);

  // Setup web server routes
  server.on("/", handleRoot);
  server.on("/style.css", []() {
    server.send(200, "text/css", "body { font-family: Arial, sans-serif; text-align: center; background-color: #f2f2f2; margin: 0; padding: 0; } h1 { color: #333; margin-top: 20%; } #heartRate { color: #e74c3c; }");
  });
  server.on("/script.js", []() {
    server.send(200, "application/javascript", "setInterval(function() { fetch('/heartRate').then(function(response) { return response.text(); }).then(function(data) { document.getElementById('heartRate').innerText = data; }); }, 1000);");
  });
  server.on("/heartRate", []() {
    server.send(200, "text/plain", String(heartRate));
  });
  server.onNotFound(redirectToCaptivePortal);

  server.begin();
  Serial.println("HTTP server started");
  Serial.println("Core 1 setup finished");
}

void loop() {
  rawValue = analogRead(EKG_PIN);
  filteredValue = highPassFilter(lowPassFilter(rawValue));

  // Calculate brightness with smoothing
  brightness = (previousBrightness * 9 + map(filteredValue * filteredValue, 0, 15000, 25, 255)) / 10;
  previousBrightness = brightness;

  // Peak detection
  if (filteredValue > THRESHOLD) {
    unsigned long currentTime = millis();
    if (currentTime - lastPeakTime > MIN_INTERVAL) {
      if (detectPeak(filteredValue)) {
        lastPeakTime = currentTime;

        // Calculate heart rate
        static unsigned long previousPeakTime = 0;
        if (previousPeakTime != 0) {
          heartRate = (prevHeartRate * 9 + (60000.0 / (currentTime - previousPeakTime))) / 10;
          prevHeartRate = heartRate;
        }
        previousPeakTime = currentTime;
      }
    }
  }

  controlLEDs(brightness, heartRate);

  delay(10);  // Adjust delay as needed
}

void loop1() {
  dnsServer.processNextRequest();
  server.handleClient();
  delay(10);
}

float lowPassFilter(float input) {
  lowPassValue += LP_ALPHA * (input - lowPassValue);
  return lowPassValue;
}

float highPassFilter(float input) {
  highPassValue = HP_ALPHA * (highPassValue + input - prevValue);
  prevValue = input;
  return highPassValue;
}

bool detectPeak(float current) {
  values[valueIndex] = current;
  valueIndex = (valueIndex + 1) % WINDOW_SIZE;

  if (values[(valueIndex + 1) % WINDOW_SIZE] > values[valueIndex] && values[(valueIndex + 1) % WINDOW_SIZE] > values[(valueIndex + 2) % WINDOW_SIZE]) {
    return true;
  }
  return false;
}

void controlLEDs(long brightness, float heartRate) {
  uint8_t red = 0, green = 0, blue = 0;

  if (heartRate < 60) {
    // Blue for low heart rate
    blue = map(heartRate, 0, 59, brightness, 0);
    green = 0;
    red = 0;
  } else if (heartRate < 100) {
    // Transition from blue to green for normal heart rate
    blue = map(heartRate, 60, 100, brightness, 0);
    green = map(heartRate, 60, 100, 0, brightness);
    red = 0;
  } else {
    // Transition from green to red for high heart rate
    blue = 0;
    green = map(heartRate, 100, 180, brightness, 0);
    red = map(heartRate, 100, 180, 0, brightness);
  }

  uint32_t color = line1.Color(red, green, blue);

  line1.fill(color, 0, COUNT1);
  line2.fill(color, 0, COUNT2);
  line3.fill(color, 0, COUNT3);

  line1.show();
  line2.show();
  line3.show();
}

void handleRoot() {
  serveHtml();
}

void handleNotFound() {
  server.send(404, "text/plain", "404: Not Found");
}

void serveHtml() {
  String html = "<!DOCTYPE html><html><head><title>Heart Rate Monitor</title>";
  html += "<link rel='stylesheet' href='/style.css'></head><body>";
  html += "<h1>Heart Rate: <span id='heartRate'>";
  html += heartRate;
  html += "</span> BPM</h1>";
  html += "<script src='/script.js'></script></body></html>";
  server.send(200, "text/html", html);
}

void redirectToCaptivePortal() {
  server.sendHeader("Location", String("http://") + WiFi.softAPIP().toString(), true);
  server.send(302, "text/plain", "");
}
