# Heart Rate Monitor with Captive Portal on Raspberry Pi Pico W - project iron heart

The project uses a Raspberry Pi Pico W to create a heart rate monitor with a captive portal that displays the heart rate data on a web page and visualize heart activity on RGB strips. The Pico W acts as an access point and serves a web page automatically when a device connects to the Wi-Fi network.

## Features

- Captive Portal: Automatically redirects connected devices to the heart rate monitor web page.
- Heart Rate Display: Shows real-time heart rate data on the web page.
- LED Visualization: Uses NeoPixel LEDs to visualize heart rate data with color transitions.

## Hardware Requirements

For custom design:

- Raspberry Pi Pico W
- NeoPixel LED strips
- EKG sensor with analog output pin
- Breadboard and connecting wires

For Iron Heart project:

- Iron heart PCB
- Raspberry Pi Pico W
- 39 x WS2812B
- EKG sensor AD8232
- JST-PH connector
- 4xAAA battery box and 4xAAA battery

## Software Requirements

- Arduino IDE
- Required Arduino libraries:
  - Adafruit NeoPixel
  - WiFi
  - WebServer
  - DNSServer
- Installed required Rpi pico boards

## Installation

1. **Set up Arduino IDE for Raspberry Pi Pico W:**

   - Follow the instructions from the [official guide](https://www.raspberrypi.org/documentation/microcontrollers/micropython.html) to set up the Arduino IDE for the Raspberry Pi Pico W.

2. **Install Required Libraries:**

   - Install the following libraries via the Arduino Library Manager:
     - Adafruit NeoPixel
     - WiFi
     - WebServer
     - DNSServer

3. **Install the Raspberry Pi Pico Boards:**

   - Open the Arduino IDE.
   - Go to `File` > `Preferences`.
   - In the `Additional Boards Manager URLs` field, enter the following URL: `https://github.com/raspberrypi/arduino-barebones/releases/download/0.0.1/package_raspberrypi_barebones_index.json`
   - Click `OK` to save the preferences.
   - Go to `Tools` > `Board` > `Boards Manager`.
   - Search for `Raspberry Pi Pico` and click on `Install` to install the boards.

4. **Flash the Code:**
   - Connect your Raspberry Pi Pico W to your computer.
   - Open the provided `src.ino` file in the Arduino IDE.
   - Select the correct board and port from the `Tools` menu.
   - Click on the `Upload` button to flash the code to the Pico W.

## Usage

1. **Connect EKG sensor:**

   - Use provided pads to connect the sensor electrodes.

2. **Power the Pico W:**

   - After flashing the code, power the Raspberry Pi Pico W using a USB cable or an external power source.

3. **Connect to Wi-Fi:**

   - On your mobile device or computer, connect to the Wi-Fi network named `IRON_HEART` with the password `1234567899`.

4. **Automatic Redirection:**
   - Once connected, the device should automatically redirect to the heart rate monitor web page. If not, open a web browser and navigate to `http://192.168.4.1`.

## Web Page

The generated web page includes:

- A heading displaying the text "Heart Rate Monitor".
- A dynamically updated section showing the current heart rate in BPM (beats per minute).
- CSS for basic styling to enhance the appearance of the web page.
- JavaScript for automatically fetching and updating the heart rate data every second.

### HTML, CSS, and JavaScript

- **HTML:** Structure of the web page including a heading and a section for displaying heart rate data.
- **CSS:** Styling to make the web page look visually appealing.
- **JavaScript:** Script to fetch the heart rate data from the server and update the web page every second.

## Functions Overview

- `setup()`: Initializes the EKG sensor and NeoPixel LED strips.
- `setup1()`: Runs on the second core to set up Wi-Fi in AP mode, starts the DNS server for the captive portal, and starts the web server.
- `loop()`: Continuously reads EKG data, processes it to detect heart rate, and updates the LEDs based on the heart rate.
- `loop1()`: Handles DNS and HTTP client requests for the captive portal.
- `lowPassFilter(float input)`: Applies a low-pass filter to the input signal.
- `highPassFilter(float input)`: Applies a high-pass filter to the input signal.
- `detectPeak(float current)`: Detects peaks in the filtered EKG signal.
- `controlLEDs(long brightness, float heartRate)`: Controls the NeoPixel LEDs to visualize the heart rate with color transitions.
- `handleRoot()`: Serves the main HTML page.
- `handleNotFound()`: Redirects any unknown requests to the captive portal.
- `serveHtml()`: Generates and serves the main HTML content for the web page.
- `redirectToCaptivePortal()`: Redirects clients to the captive portal.

## Notes

- Ensure that the EKG sensor and NeoPixel LED strips are correctly connected to the Raspberry Pi Pico W.
- The Wi-Fi credentials (`ssid` and `password`) can be changed to your preference.
